"""
Created on 6 December 2022

@author: C. Guychard <christophe.guychard@tekfor.cloud>
@author: D. Couppé <damien.couppe@tekfor.cloud>
@copyright: ©2022 Article 714, Tekfor
@license: LGPL v3
"""

from os.path import exists as pathexists
import socket
import json
import logging

from ..commons import BaseScript
from ..commons.actions import execute_shell_command


class HealthChecker(BaseScript):
    """
    Main class for the python based healtcheck script
    """

    def _is_container_status_ok(self, sock_file_name: str) -> bool:
        try:
            sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            sock.connect(sock_file_name)
            sock.send(b"GET / HTTP/1.1\r\nHost:localhost\r\n\r\n")
            response = ""
            response_part = True
            while response_part:
                response_part = sock.recv(4096)
                response += response_part.decode()
            sock.close()

            fields = response.split("\r\n")
            body = fields[-1]
            json_body = json.loads(body)
            logging.info(
                f'Status from containerstarter is: {json_body.get("status", "none")}, '
                f'in mode: {json_body.get("mode", "--")}'
            )

            return json_body.get("status", "none") == "ok"
        except Exception as exc:
            logging.error(f"Failed to check entrypoint status: {exc}")
            return False

    def _check_runit_services(self) -> bool:
        all_ok = True
        return_code, output = execute_shell_command(
            command="update-service --list")
        output = output.strip()
        svc_number = 0
        if return_code == 0:
            for service_name in output.split("\n"):
                if service_name:
                    svc_number += 1
                    return_code, output = execute_shell_command(
                        command=f"sv status {service_name}")
                    status = output.split(":")
                    all_ok = all_ok and status[0] == "run"
                    if status[0] != "run":
                        logging.warning(
                            f"Healtcheck: service {service_name} should be running...")
        else:
            logging.error("ERROR: could not get list of runit services")
            return False
        if all_ok:
            logging.info(f"All runit ({svc_number}) services are running")
        return all_ok

    def run(self) -> bool:

        healthy = self.check_and_read_config_file()

        # Check container status from containerstarter
        for sock_type in self.config.get("socket", {}):
            sock_conf = self.config.get("socket").get(sock_type)
            if sock_type == "file":
                filename = sock_conf.get("path")
                if pathexists(filename):
                    healthy = healthy & self._is_container_status_ok(filename)

        # Check running processes
        healthy = healthy & self._check_runit_services()
        return healthy
