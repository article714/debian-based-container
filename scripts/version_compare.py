from packaging import version
import argparse


def init_args():
    """
    Parse args for command line
    """
    parser = argparse.ArgumentParser(description="Compare two versions")
    parser.add_argument(
        "--left",
        dest="left_ver",
        default=None,
        required=True,
    )
    parser.add_argument(
        "--right",
        dest="right_ver",
        default=None,
        required=True,
    )

    return parser.parse_args()


if __name__ == "__main__":

    myargs = init_args()

    if myargs and myargs.left_ver and myargs.right_ver:
        left_ver = version.parse(myargs.left_ver)
        right_ver = version.parse(myargs.right_ver)
        if left_ver > right_ver:
            print(">")
        elif left_ver < right_ver:
            print("<")
        else:
            print("=")
    else:
        print("ERROR")
        exit(-1)
