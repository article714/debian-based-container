"""
Created on 5 December 2022

@author: C. Guychard <christophe.guychard@tekfor.cloud>
@author: D. Couppé <damien.couppe@tekfor.cloud>
@copyright: ©2022 Article 714, Tekfor
@license: LGPL v3
"""
import logging


class ContainerState:
    """
    The network server that will listen to requests on unix-socket or network socket
    and provide information about container mode / state
    """

    def __init__(self) -> None:
        self.status = "ok"
        self.current_mode = "--"
        self.target_mode = "--"
        self.last_error = None

    def set_error(self, error: str):
        self.last_error = error
        self.status = "ko"
        logging.error(error)
