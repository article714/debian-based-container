"""
Created on 6 December 2022

@author: C. Guychard <christophe.guychard@tekfor.cloud>
@author: D. Couppé <damien.couppe@tekfor.cloud>
@copyright: ©2022 Article 714, Tekfor
@license: LGPL v3
"""

from types import FrameType
from typing import Optional
import os
import signal
import logging
import logging.config
from .server import StateServer
from .state import ContainerState
from ..commons import SPECIAL_MODES, BaseScript
from ..commons.actions import execute_shell_action


class ContainerStarter(BaseScript):
    """
    Main class for the python based entrypoint
    """

    def __init__(self, config_file_path: str) -> None:
        super().__init__(config_file_path)

        self.run_idle = True
        self.containerstate = ContainerState()
        self.statesrv = None

    def run(self) -> None:
        signal.signal(signal.SIGTERM, self.kill_idle)
        signal.signal(signal.SIGINT, self.kill_idle)
        signal.signal(signal.SIGQUIT, self.kill_idle)

        self.check_and_read_config_file()

        # load logging configuration
        self.setup_logging()

        go_idle = True
        try:
            self.set_current_mode("starting")
            self.set_target_mode("starting")
            self.start_state_server()
            go_idle = self._run(self.config.get("start_mode", "idle"))
        except Exception as exc:  # noqa:E722
            logging.exception(f"This unmanaged error occurred: {exc}")
            self.containerstate.set_error(f"Exception occurred: {type(exc)} - {exc.args}")
        finally:
            if go_idle:
                self.idle()
            try:
                # run stop mode if defined
                self.set_current_mode("stopping")
                self._run(self.config.get("stop_mode", "stop"))
            except Exception as exc:  # noqa:E722
                logging.exception(f"While STOPPING, this unmanaged error occurred: {exc}")
                self.containerstate.set_error(
                    f"While STOPPING Exception occurred: {type(exc)} - {exc.args}"
                )
            finally:
                self.stop_state_server()

    def _run(self, target_mode) -> bool:
        """
        returns true if container should go idle after run
        """

        # configure start mode => idle by default
        next_mode = target_mode
        if self.is_config_file_ok:
            while self.is_config_file_ok and next_mode not in SPECIAL_MODES:
                self.set_target_mode(next_mode)
                if next_mode not in SPECIAL_MODES:
                    self.read_target_mode(next_mode)
                    if self.target_mode_content:
                        next_mode = self.execute_target_mode()
                    else:
                        raise Exception(f"Definition file is incorrect for mode {next_mode}")
            return next_mode == "idle"
        raise Exception("Config file is incorrect")

    def set_current_mode(self, mode) -> None:
        logging.info(f"Entering mode: {mode}")
        self.containerstate.current_mode = mode

    def set_target_mode(self, mode) -> None:
        logging.info(f"Targeting mode: {mode}")
        self.containerstate.target_mode = mode

    def start_state_server(self) -> None:
        self.statesrv = StateServer(self.config, self.containerstate)
        logging.info("Starting state server")
        self.statesrv.start_servers()

    def stop_state_server(self) -> None:
        if self.statesrv:
            logging.info("Stopping state server")
            self.statesrv.stop_servers()

    def read_target_mode(self, target_mode: str) -> None:
        file_path = os.path.join(
            self.config["modes_directory"],
            f"{target_mode}.yaml",
        )
        logging.info(f"Read target_mode file {file_path}")
        self.set_target_mode(target_mode)
        self.target_mode_content = self._check_and_read_yaml_file(file_path)

    def execute_target_mode(self) -> bool:
        """
        returns next target mode depending on execution result:
        either on_failure or on_success mode
        """
        if not self.target_mode_content:
            return True

        return_code = 0

        # execution actions for the mode
        for action in self.target_mode_content["actions"]:
            for action_type, commands in action.items():
                if action_type == "shell":
                    return_code = execute_shell_action(commands)
                    if return_code != 0:
                        msg = f"shell commande return with a non-zero code : {return_code}"
                        logging.warning(msg)
                        self.containerstate.set_error(msg)
                        break
                    else:
                        self.set_current_mode(self.containerstate.target_mode)

        next_target_mode = self.target_mode_content.get("next_mode", False)

        if next_target_mode:
            on_success_mode = next_target_mode.get("on_success", "idle")
            on_failure_mode = next_target_mode.get("on_failure", "idle")
        else:
            on_success_mode = "idle"
            on_failure_mode = "idle"

        if return_code == 0:
            logging.info(f"Success: next mode is {on_success_mode}")
            return on_success_mode
        else:
            logging.info(f"Failure: next mode is {on_failure_mode}")
            return on_failure_mode

    def idle(self) -> None:
        self.set_current_mode("idle")
        while self.run_idle:
            signal.pause()

    def kill_idle(
        self, signum: int, frame: Optional[FrameType]
    ) -> None:  # pylint: disable=unused-argument
        logging.info(f"Receive kill_idle signal {signum}")
        self.run_idle = False
