ARG BASE_DISTRO
ARG BASE_VERSION
FROM ${BASE_DISTRO}:${BASE_VERSION}
LABEL maintainer="C. Guychard<christophe@article714.org>"

ARG BASE_DISTRO
ARG BASE_VERSION
ARG PRODUCT_VERSION
ARG IMAGE_VERSION


# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
# directory that contains active service configurations (runit)
ENV SVDIR /container/config/services

# directory that contains service defintions (runit)
ENV SERVICEDIR /container/services

# Add /container/config/ to PYTHONPATH

ENV PYTHONPATH $PYTHONPATH:/container/tools

# Container tooling

COPY container /container
COPY dependencies /container/dependencies

# container building

RUN /container/build.sh

# healthcheck
HEALTHCHECK --interval=120s --timeout=2s --retries=5 CMD python3 /container/healthcheck.py /container/config/entrypoint/config.yml

USER root

# entrypoint  & default command
ENTRYPOINT [ "python3", "/container/containerstarter.py", "/container/config/entrypoint/config.yml" ]
