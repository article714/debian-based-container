#!/bin/bash

# set -x

export SVDIR=/container/config/services/

services=$(update-service --list)

for srv in ${services}; do
    /usr/bin/sv status ${srv}
    if [ $? -ne 0 ]; then
        exit 1
    fi
done

exit 0
