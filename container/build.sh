#!/bin/bash

apt-get update
apt-get upgrade -yq
apt-get install -y locales
localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

export LANG=en_US.utf8
export LC_ALL=en_US.utf8
export SVDIR=/container/config/services
export SERVICEDIR=/container/services

# Install needed runit/rsyslog packages
LC_ALL=C DEBIAN_FRONTEND=noninteractive apt-get install -qy --no-install-recommends runit rsyslog logrotate

if [ -f "/container/dependencies/packages_to_install.txt" ]; then
  packages_to_install=$(cat /container/dependencies/packages_to_install.txt)
  # shellcheck disable=SC2086
  LC_ALL=C DEBIAN_FRONTEND=noninteractive apt-get install -qy --no-install-recommends runit rsyslog logrotate ${packages_to_install}
  # shellcheck disable=SC2181
  if [ $? -ne 0 ]; then
    echo "ERROR: Failed to install distro specific dependencies"
    exit 1
  fi
fi

if [ -f "/container/dependencies/${BASE_DISTRO}/packages_to_install.txt" ]; then
  packages_to_install=$(cat /container/dependencies/"${BASE_DISTRO}"/packages_to_install.txt)
  # shellcheck disable=SC2086
  LC_ALL=C DEBIAN_FRONTEND=noninteractive apt-get install -qy --no-install-recommends runit rsyslog logrotate ${packages_to_install}
  # shellcheck disable=SC2181
  if [ $? -ne 0 ]; then
    echo "ERROR: Failed to install version specific dependencies"
    exit 1
  fi
fi

# Install needed python packages

if [ -f "/container/dependencies/requirements.txt" ]; then
  if ! python3 -m pip install -r /container/dependencies/requirements.txt; then
    echo "ERROR: Failed to install python common deps"
    exit 1
  fi
fi

if [ -f "/container/dependencies/${BASE_DISTRO}/requirements.txt" ]; then
  if ! python3 -m pip install -r /container/dependencies/"${BASE_DISTRO}"/requirements.txt; then
    echo "ERROR: Failed to install python version specific deps"
    exit 1
  fi
fi


# Add Syslog user
groupadd -g 110 syslog
adduser --no-create-home --disabled-password --shell /usr/sbin/nologin --gecos "" --uid 104 --gid 110 syslog

chgrp syslog /var/log
chmod 775 /var/log

# Fiwing permissions

chmod +x /container/tools/*
chmod +x /container/postrotate-script
chmod +x /container/check_running
#--
# config files redirection

rm -f /etc/rsyslog.conf
ln -s /container/config/rsyslog.conf /etc/rsyslog.conf

rm -f /etc/logrotate.conf
ln -s /container/config/logrotate.conf /etc/logrotate.conf

rm -rf /etc/crontab /etc/cron.d
ln -s /container/config/crontab /etc/crontab
ln -s /container/config/cron.d /etc/cron.d

#--
# services activation
mkdir -p ${SVDIR}
svc_defs=$(find ${SERVICEDIR} -mindepth 1 -maxdepth 1 -type d)
for svc in ${svc_defs}; do
    update-service --add "${svc}"
done

#--
# Cleaning

if [ -f "/container/dependencies/${BASE_DISTRO}/packages_to_purge.txt" ]; then
  packages_to_purge=$(cat /container/dependencies/"${BASE_DISTRO}"/packages_to_purge.txt)
  apt-get purge -y --allow-remove-essential "${packages_to_purge}"
fi


if [ -f "/container/dependencies/packages_to_purge.txt" ]; then
  packages_to_purge=$(cat /container/dependencies/packages_to_purge.txt)
  apt-get purge -y --allow-remove-essential "${packages_to_purge}"
fi


apt-get -yq clean
apt-get -yq --allow-remove-essential autoremove
rm -rf /var/lib/apt/lists/*
# cleanup useless cron jobs
rm -f /etc/cron.daily/passwd /etc/cron.daily/dpkg /etc/cron.daily/apt-compat
# truncate logs
find /var/log -type f -exec truncate --size 0 {} \;
rm -rf build