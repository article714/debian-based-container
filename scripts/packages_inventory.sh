#!/bin/bash
#
# generates a simple inventory txt file for installed software (dpkg) packages
#

BASE_VER=0.10.0

DISTROS="debian-bullseye-slim ubuntu-22.10 python-3.9"

for distro in $DISTROS; do
  docker run  --rm article714/debian-based-container:${distro}-${BASE_VER} dpkg -l  | grep "^ii" | sed "s/\W\W\W*/-/g" > installed_packages_${distro}.txt
done

# diff installed_packages_debian.txt installed_packages_python.txt