import socket
import json
from sys import argv
from sys import exit as sysexit

sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
sock.connect("container/config/entrypoint/container.sock")
sock.send(b"GET / HTTP/1.1\r\nHost:i.dont.care\r\n\r\n")
response = ""
response_part = True
while response_part:
    response_part = sock.recv(4096)
    response += response_part.decode()
sock.close()

fields = response.split("\r\n")
body = fields[-1]
json_body = json.loads(body)

checks = json.loads(' '.join(argv[1:]))
for field in checks:
    print(f"checking: {field} {json_body.get(field)}")
    if checks[field] != json_body.get(field, False):
        print(
            f"FAILED: {field} is not correct: {json_body.get(field)} / {checks[field]}")
        sysexit(1)
