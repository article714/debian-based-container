################################################################################
# Style file for markdownlint.
#
# https://github.com/markdownlint/markdownlint/blob/master/docs/configuration.md
#
# This file is referenced by the project `.mdlrc`.
################################################################################

all

rule 'MD013', :line_length => 130
rule 'MD029', :style => :ordered
rule 'MD046', :style => :fenced
rule 'MD007', :indent => 2
exclude_rule 'MD041'
exclude_rule 'MD026'