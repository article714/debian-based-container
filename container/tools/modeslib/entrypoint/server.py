"""
Created on 5 December 2022

@author: C. Guychard <christophe.guychard@tekfor.cloud>
@author: D. Couppé <damien.couppe@tekfor.cloud>
@copyright: ©2022 Article 714, Tekfor
@license: LGPL v3
"""


from socketserver import TCPServer, UnixStreamServer, ThreadingMixIn
from http.server import SimpleHTTPRequestHandler

import json
import logging
from threading import Thread
from os import unlink
from os.path import exists as pathexists


class StateRequestHandler(SimpleHTTPRequestHandler):
    """
    Request handler that will provide information
    about container state & mode to requester
    """

    def do_GET(self):
        """Serve a GET request."""

        if not self.client_address:
            self.client_address = ("localhost", "80")
        self.send_response(200)
        self.send_header("Content-type", "txt/json")
        self.end_headers()
        # To Do provide information about state
        server_state = self.server.get_state()
        self.wfile.write(
            json.dumps(
                {
                    "status": server_state.status,
                    "mode": server_state.current_mode,
                    "failed_mode": server_state.target_mode if server_state.last_error else None,
                    "error": server_state.last_error,
                }
            ).encode("utf-8")
        )


class ThreadedUnixStreamServer(ThreadingMixIn, UnixStreamServer):
    def set_state(self, stateobject):
        """sets container state object"""
        self.containerstate = stateobject

    def get_state(self):
        """returns container state object"""
        return self.containerstate


class ThreadedTCPServer(ThreadingMixIn, TCPServer):
    def set_state(self, stateobject):
        """sets container state object"""
        self.containerstate = stateobject

    def get_state(self):
        """returns container state object"""
        return self.containerstate


class StateServer:
    """
    The network server that will listen to requests on unix-socket or network socket
    and provide information about container mode / state
    """

    def __init__(self, config, stateobject):
        """
        Create a new instance of state server using a config object thats
        provides information on socket to create
        and a stateobject that will provide information about current container
        mode & state
        """
        self.config = config
        self.containerstate = stateobject

    def start_servers(self):
        """
        Starts HTTP Server that will serving state information to external programs
        """

        # Create self.servers
        self.servers = []
        for sock_type in self.config.get("socket", {}):
            sock_conf = self.config.get("socket").get(sock_type)
            if sock_type == "file":
                filename = sock_conf.get("path")
                if pathexists(filename):
                    unlink(filename)

                server = ThreadedUnixStreamServer(filename, StateRequestHandler)
                server.set_state(self.containerstate)
                self.servers.append(server)
            elif sock_type == "tcp":
                server = ThreadedTCPServer(
                    (sock_conf.get("host"), sock_conf.get("port")),
                    StateRequestHandler,
                )
                server.set_state(self.containerstate)
                self.servers.append(server)
            else:
                logging.error(f"Can Not find socket type: {sock_type}")
                return False

        # Start Dameons
        for server in self.servers:
            server_thread = Thread(target=server.serve_forever)
            server_thread.daemon = True
            server_thread.start()

        return True

    def stop_servers(self):
        """
        Stops HTTP Server
        """
        for server in self.servers:
            server.shutdown()

        for sock_type in self.config.get("socket", {}):
            sock_conf = self.config.get("socket").get(sock_type)
            if sock_type == "file":
                filename = sock_conf.get("path")
                if pathexists(filename):
                    unlink(filename)
