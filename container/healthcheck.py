"""
Created on 15 December 2022

@author: C. Guychard <christophe.guychard@tekfor.cloud>
@author: D. Couppé <damien.couppe@tekfor.cloud>
@copyright: ©2022 Article 714, Tekfor
@license: LGPL v3
"""

import logging
import sys
from modeslib.healthcheck import HealthChecker

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    if len(sys.argv) == 2:
        CONFIG_FILE_PATH = sys.argv[1]
        HEALTH_CHECKER = HealthChecker(CONFIG_FILE_PATH)
        healthy = HEALTH_CHECKER.run()
        if healthy:
            sys.exit(0)
        else:
            sys.exit(1)
    else:
        logging.error(
            f"{__file__} must be called with config file path as argument")
        sys.exit(1)
