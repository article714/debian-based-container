# container-base-debian

Basic image to build multi-processes container based on [Debian](https://hub.docker.com/_/debian), [Python](https://hub.docker.com/_/python) or [Ubuntu](https://hub.docker.com/_/ubuntu) official images with basic tooling, including:

- [runit](http://smarden.org/runit/)
- [rsyslog](https://www.rsyslog.com/)
- [python](https://python.org), version 3.x

This image is meant to be used as base for building others that:

- may host multiple processes (services) manageable using [runit](http://smarden.org/runit/);
- can be target by [ansible](https://ansible.com) tools (role or playbook);
- may need to define several [modes](documentation/state_and_mode_mgmt.md) for the container.

Images are built on dedicated gitlab-runner and pushed to [hub.docker.com](https://hub.docker.com/r/article714/debian-based-container).

## Image versions and Tags

TODO:

- `debian-*` => debian based with python version from distro
- `ubuntu-*` => ubuntu based with python version from distro
- `python-*` => from python base image (using debian-\*-slim)

## Branching policy

Three main branches are used to develop `debian-based-container`:

- **master**, main development branch, used for publishing _:preview_ docker images
- **production**, used for publishing _:latest_ docker images
- **test**, used for developments requiring to skip certain phases of the build

Official releases (except _:latest_) are produced from a ** tag ** in the format _X.Y.Z_.

**WARNING** This repository changed its organization several times. Thus it contains several obsolete branches like those that were targeting different versions of debian.
E.g.: `buster` branch is used to work on a container based on a [debian](https://debian.org) `buster` based container.

Le last (and current) organization is back to a more standard way of doing things, and makes use of [Gitlab CI parallel feature](https://docs.gitlab.com/ee/ci/yaml/index.html#parallel) to
build image for all supporterd debian/ubuntu/... versions at the same time.
