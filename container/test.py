from multiprocessing.connection import Client

address = "/container/config/entrypoint_mode.sock"
conn = Client(address)
conn.send("close")
# can also send arbitrary objects:
# conn.send(['a', 2.5, None, int, sum])
conn.close()
