# Release Notes

## Work in Progress (<will be version 0.12.0>)

- moved to main branch of build-tools
- added pre-commit support
- deprecates buster based versions (issue #5)

## Version 0.11.0

- optimized postrotate logrotate script
- fixed issue #4 related to default healthcheck script
- fixed issues #2 and #3, related to service management scripts (`tools/add_all_services.sh`,
  `tools/remove_all_services.sh`)
- added a way to define the ["mode"](documentation/state_and_mode_mgmt.md) in which container
  is running (issue #1)
- added some documentation

## Version 0.10.0

- supported distros and versions are: debian buster & bullseye, ubuntu 20.04 & 22.04 (LTS), python
  base image 3.9 & 3.10
- added a simple script for replacing lsb_release functionality
- removed support for lsb_release as it conflicts with python version brought by python-\* base
  images
- only supports debian distro with latest debian + some "latest" python version (To Be documented)
- added a way to add or remove specific packages, per distro/version
- added builds for ubuntu or python base images
- remove _essential_ but useless packages such as `e2fsprogs`
- refactor build system for multi-versions

## Version 0.9.0

- changed the way runit is used, service definition in /container/services, service activation (SVDIR)
  in /container/config/services
