"""
Created on 15 December 2022

@author: C. Guychard <christophe.guychard@tekfor.cloud>
@author: D. Couppé <damien.couppe@tekfor.cloud>
@copyright: ©2022 Article 714, Tekfor
@license: LGPL v3
"""

from time import sleep
import logging
import logging.config
import os
import subprocess


def execute_shell_action(commands: list) -> int:
    """
    logs out stdout & stderr for commands and returns
    last return code of last executed command
    """
    proc = _run_shell_script(commands=commands)
    while True:
        output = proc.stdout.readline()
        return_code = proc.poll()
        if output == "" and return_code is not None:
            break
        if output:
            logging.info(output.strip())
        sleep(0.01)
    return return_code


def execute_shell_command(command: str) -> list:
    """
    runs a shell command and returns (return code, output string)
    """
    proc = _run_shell_script(cmd=command)
    output = ""
    while True:
        new_output = proc.stdout.readline()
        output = output + new_output
        return_code = proc.poll()
        if new_output == "" and return_code is not None:
            break
        sleep(0.01)
    return return_code, output


def _run_shell_script(commands: list = [], cmd: str = None):
    """
    Runs list of shell commands from the `shell` action
    """
    if commands:
        command = "\n".join(commands)
    else:
        command = cmd
    proc = subprocess.Popen(
        ["/bin/bash", "-c", command],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        universal_newlines=True,
    )
    # reading on stdout should not be blocking as
    os.set_blocking(proc.stdout.fileno(), False)
    return proc
