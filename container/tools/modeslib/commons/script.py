"""
Created on 15 December 2022

@author: C. Guychard <christophe.guychard@tekfor.cloud>
@author: D. Couppé <damien.couppe@tekfor.cloud>
@copyright: ©2022 Article 714, Tekfor
@license: LGPL v3
"""

import os
import yaml
import logging
import logging.config


class BaseScript:
    """
    Main class for the python based entrypoint
    """

    def __init__(self, config_file_path: str) -> None:
        self.config_file_path = config_file_path
        self.config = {}
        self.containerstate = None
        self.is_config_file_ok = False

    def _log_error(self, msg: str):
        if self.containerstate:
            self.containerstate.set_error(msg)
        logging.error(msg)

    def check_and_read_config_file(self) -> bool:
        self.config = self._check_and_read_yaml_file(self.config_file_path)
        self.is_config_file_ok = self.config and self.check_config_file()
        return self.is_config_file_ok

    def _check_and_read_yaml_file(self, file_path) -> dict:
        content = False
        if os.path.exists(file_path):
            try:
                with open(file_path) as f:
                    content = yaml.safe_load(f)
            except yaml.YAMLError as exc:
                self._log_error(f"Yaml file not loaded {file_path} --> {exc}")
        else:
            self._log_error(f"Yaml file path {file_path} doesn't exists or is not accessible")
        return content

    def check_config_file(self) -> bool:
        is_config_file_ok = True
        if not self.config.get("start_mode"):
            is_config_file_ok = False

            self._log_error("No start_mode defined in CONFIG_FILE")

        if not self.config.get("modes_directory"):
            is_config_file_ok = False
            self._log_error("No modes_directory defined in CONFIG_FILE")

        return is_config_file_ok

    def setup_logging(self):
        if self.config.get("logging"):
            try:
                logging.config.dictConfig(self.config["logging"])
            except (ValueError, TypeError, AttributeError, ImportError):
                logging.exception("Failed to configure logging sub-system:")
