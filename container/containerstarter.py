"""
Created on 5 December 2022

@author: C. Guychard <christophe.guychard@tekfor.cloud>
@author: D. Couppé <damien.couppe@tekfor.cloud>
@copyright: ©2022 Article 714, Tekfor
@license: LGPL v3
"""

import logging
import sys
from os import execvp
from modeslib.entrypoint import ContainerStarter

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    if len(sys.argv) == 2:
        CONFIG_FILE_PATH = sys.argv[1]
        CONTAINER_STARTER = ContainerStarter(CONFIG_FILE_PATH)
        CONTAINER_STARTER.run()
    elif len(sys.argv) > 2:
        # execute command if one is given
        execvp(sys.argv[2], sys.argv[2:])
    else:
        logging.error(
            f"{__file__} must be called with entrypoint config file path as argument")
        sys.exit(1)
