from os_release import OsRelease
import argparse
from configobj import ConfigObj


def init_args():
    """
    Parse args for command line
    """

    options = {
        "i": "id",
        "d": "description",
        "r": "release",
        "c": "codename",
        "a": "all",
    }
    messages = {
        "i": "show distributor ID",
        "d": "show description of this distribution",
        "r": "show release number of this distribution",
        "c": "show code name of this distribution",
        "a": "show all of the above information",
    }

    parser = argparse.ArgumentParser(
        description="Provide information about operating system identification"
    )
    filemtx = parser.add_mutually_exclusive_group(required=True)

    for key in options:
        filemtx.add_argument(
            f"--{options[key]}",
            f"-{key}",
            dest=f"{options[key]}",
            default=None,
            help=f"{messages[key]}",
            action="store_true",
        )
    parser.add_argument(
        "--file",
        "-f",
        dest="filename",
        default="/etc/os-release",
        help="filename to parse",
    )

    return parser.parse_args()


if __name__ == "__main__":

    myargs = init_args()

    release_data = ConfigObj(myargs.filename)

    my_release = OsRelease.from_dict(release_data)

    if myargs:
        if myargs.id or myargs.all:
            print(my_release.id.lower())
        if myargs.description or myargs.all:
            print(my_release.pretty_name)
        if myargs.release or myargs.all:
            print(my_release.version_id)
        if myargs.codename or myargs.all:
            print(my_release.version_codename)
    else:
        print("ERROR")
        exit(-1)
