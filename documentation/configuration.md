# Container configuration

All container configuration and tooling is stored in `/container` directory, which have a conventional layout:

- `/container`, base directory containing custom tooling, configuration files and state information;
- `/container/config`, is meant to be bound to a volume as it contains configuration files & directories and transient state & mode data;
- `/container/services`, contains [run scripts](http://smarden.org/runit/runscripts.html) (_service definitions_) used by [`runit`](http://smarden.org/runit), that can be (de)activated using `update-service`;
- `/container/entypoint`, entrypoint scripts to be run depending on [container mode](../documentation/state_and_mode_mgmt.md)
