#!/bin/bash

export LANG=en_US.utf8
export SVDIR=/container/config/services
export SERVICEDIR=/container/services
#--
# services activation
mkdir -p ${SVDIR}
svc_defs=$(find ${SERVICEDIR} -mindepth 1 -maxdepth 1 -type d)
for svc in ${svc_defs}; do
    update-service --add ${svc}
done
