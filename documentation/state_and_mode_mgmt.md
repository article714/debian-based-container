# State and mode management

`debian-based-container`provides a custom, python3 based, entrypoint (_containerstarter_) that provides a _mode_ management feature.

When container entrypoint is not overrided, the `/container/containerstarter.py` script will be the first process to be run. That means container will stop when _containerstarter_ stops.

It is getting its [configuration](#configuring-mode-management) from `/container/config/entrypoint/config.yml` file and
is responsible for:

- providing health information to [healtcheck](#) (`/container/healthcheck.py`) command through [state server](#querying-current-mode-state-server);
- transitioning through the successive [modes](#defining-custom-modes), i.e. execute specified actions for the current target mode, and select next mode to be targeted.

The first mode to be transitioned to, is defined by the `start_mode` entry in [configuration file](#configuring-mode-management).

## Why a mode management?

We needed to configure how a Docker container would act when starting up:

- which action to take,
- what happen in case of failure,
- provide information about which phase/mode failed, and why,
- go _idle_ without stopping in case of failure,
- ...

## What is a mode ?

A _mode_ is essentially a tag and list of actions to execute before transitioning to another _mode_. It translates the internal state of the _containerstarter_ process that manages (starts/stops) other processes in the container.

The mode in which _containerstarter_ is mostly independent from container state (whatever the CRI is: Docker, containerd, ...), even if it might act on container state via [healthcheck command](#modes-and-healthcheck-information).

A mode is defined in a _mode file_, in yaml format, which name is built from mode name, i.e.: `<mode name>.yaml`. See [below](#defining-custom-modes) for its format.
When transitioning to a given mode, _containerstarter_ reads the yaml file to discover:

- which commands (_actions_) need to be run,
- which mode to transition to in case of success or failure.

_containerstarter_ sequentially transitions from mode to another until next target is one of:

- **idle**, process will wait for a signal to be received before stopping (container should stop);
- **stop**, process will exit after running `stop_mode` if specified.

Two other special modes are defined, to represents transient states:

- **starting**, no custom target_mode set yet, entrypoint starts its execution.
- **stopping**, container will execute stop_mode.

**No special mode name should be used to name custom modes**.

## Defining custom modes

To define a new mode, a single yaml file, named after mode name has to be written. Following is an example file:

```yaml
---
actions:
  - shell:
      - PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/bin
      - export SVDIR=/container/config/services
      - /container/tools/add_all_services.sh
next_mode:
  on_success: runsvdir # default mode is "idle"
  on_failure: idle # default mode is "idle"
```

## Configuring mode management

Location of configuration file should be `/container/config/entrypoint/config.yml`, unless you override the container entrypoint.

The configuration file layout (yaml) is as follow:

- ```yaml
  socket:
    file:
      path: /container/config/entrypoint/container.sock
    tcp:
      host: 0.0.0.0
      port: 9100
  ```
  configure on which socket (either unix or standard network socket) [state server](#querying-current-mode-state-server) should be listening.

* ```yaml
  start_mode: exitdefault
  ```

  define the first mode to be transitioned to.

* ```yaml
  stop_mode: teststopmode
  ```

  define the mode to be transitioned to when entrypoint receives a signal that urges it to stop (_SIGTERM_, _SIGINT_ or _SIGQUIT_).

* ```yaml
  modes_directory: tests/modes
  ```
  define the directory where to look for [mode files](#defining-custom-modes).

- ```yaml
  logging: #...
  ```
  logging configuration in yaml format, used to configure python logging subsystem
  using dictConfig (see https://docs.python.org/3/library/logging.config.html#logging.config.dictConfig)

## Modes and healthcheck information

The default healthcheck command for container (`/container/healtcheck.py`) uses state information from _containerstarter_ provided by [state-server](#querying-current-mode-state-server) when available.

If _containerstarter_ encounters an error, its status will be set to _ko_ and container will be reported to be un-healthy.

## Querying current mode: state server

The _state server_ is a very simple http server that migh listen on an Unix socket or standard network socket for get requests.
For now, whatever the requested URL, it will answer with a simple json document reporting _containerstarter_ internal state:

```json
{
    "status": "ok" | "ko",
    "mode": current_mode // the mode we are in,
    "failed_mode": some_mode // last mode where we met an issue,
    "error": "I did failed" // last error message,
}
```
